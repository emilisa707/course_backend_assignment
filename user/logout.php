<?php

SESSION_START();

SESSION_UNSET($_SESSION);

unset($_SESSION['user_id']);

SESSION_DESTROY();

header("Location: http://localhost/course_backend_assignment/");

?>