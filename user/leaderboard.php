<?php
SESSION_START();
include("../database.php");
$db = new Database();
$user_id = (isset($_SESSION['user_id'])) ? $_SESSION['user_id'] : "";

if($user_id)
{
   $result = $db->execute("SELECT * FROM reg_user WHERE user_id = '".$user_id."' ");

   if(!$result)
   {
       header("Location: http://localhost/course_backend_assignment/");
   }

   $userdata = $db->get("SELECT reg_user.user_id as user_id, reg_user.nama as nama,
                       reg_user.kota_tinggal as kota_tinggal

                       from reg_user WHERE reg_user.user_id = '".$user_id."' "); 
                                     
   $userdata = mysqli_fetch_assoc($userdata);  
}

else
{
   header("Location: http://localhost/course_backend_assignment/");
}

$notification = (isset($_SESSION['notification'])) ? $_SESSION['notification'] : "";

if($notification)
{
   echo $notification;
   unset($_SESSION['notification']);   
}

?>

PAGE : LEADERBOARD

<table border=1>

   <tr>

       <td>MENU</td>

       <td><a href="http://localhost/course_backend_assignment/user/">HOME</a></td>

       <td><a href="http://localhost/course_backend_assignment/user/statistik.php">STATISTIK</a></td>       

       <td><a href="http://localhost/course_backend_assignment/user/leaderboard.php">LEADERBOARD</a></td>

       <td><a href="http://localhost/course_backend_assignment/user/logout.php">LOGOUT</a></td>

   </tr>

</table>

<br>

<form action="http://localhost/course_backend_assignment/user/leaderboard.php" method='GET'>

       Pilih Game

       <select name="game_id">

           <?php

           $gamedata = $db->get("SELECT game_id,nama_game FROM game");                                

           while($row = mysqli_fetch_assoc($gamedata))

           {

               ?>

               <option value="<?php echo $row['game_id']?>"><?php echo $row['nama_game']?></option>

               <?php

           }

           ?>

       </select>

       <input type="submit" value="Tampilkan Leaderboard">

</form>

<?php

if(isset($_GET['game_id']))

{

   echo "LEADERBOARD GAME ID :".$_GET['game_id'];

   ?>

   <table border=1>

   <tr><td>NO</td><td>NAMA</td><td>SCORE</td></tr>

   <?php

   $leaderboarddata = $db->get("SELECT reg_user.nama as nama, max(leaderboard.score) as score FROM reg_user, leaderboard, game_user, game WHERE reg_user.user_id = game_user.user_id AND game_user.game_id = ".$_GET['game_id']." GROUP BY reg_user.user_id ORDER BY score DESC");

   $no = 0;

   while($row = mysqli_fetch_assoc($leaderboarddata))

   {

       $no++;

       ?>

       <tr>

       <td><?php echo $no?></td>

       <td><?php echo $row['nama'] ?></td>

       <td><?php echo $row['score']?></td>               

       </tr>

       <?php

   }

   ?>

   </table>

   <?php

}

?>