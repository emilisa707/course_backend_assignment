-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Nov 2019 pada 15.33
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `course_backend_db_assignment`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `game`
--

CREATE TABLE `game` (
  `game_id` int(16) NOT NULL,
  `nama_game` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `game`
--

INSERT INTO `game` (`game_id`, `nama_game`) VALUES
(1, 'Game-001'),
(2, 'Game-002'),
(3, 'Game-003'),
(4, 'Game-004'),
(5, 'Game-005');

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `game001_leaderboard`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `game001_leaderboard` (
`email` varchar(255)
,`nama` varchar(100)
,`kota` varchar(50)
,`score` int(255)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `game_user`
--

CREATE TABLE `game_user` (
  `game_user_id` int(16) NOT NULL,
  `user_id` varchar(16) NOT NULL,
  `game_id` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `game_user`
--

INSERT INTO `game_user` (`game_user_id`, `user_id`, `game_id`) VALUES
(1, '1000000000000001', 1),
(2, '1000000000000002', 2),
(3, '1000000000000003', 3),
(4, '1000000000000001', 4),
(5, '1000000000000002', 5),
(6, 'admin', 1),
(7, 'admin', 2),
(8, 'admin', 3),
(9, 'admin', 4),
(10, 'admin', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `leaderboard`
--

CREATE TABLE `leaderboard` (
  `id_leaderboard` int(16) NOT NULL,
  `game_user_id` int(16) NOT NULL,
  `score` int(255) NOT NULL,
  `level` int(255) NOT NULL,
  `waktu_submit` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `leaderboard`
--

INSERT INTO `leaderboard` (`id_leaderboard`, `game_user_id`, `score`, `level`, `waktu_submit`) VALUES
(2, 1, 100, 10, '2019-11-04 13:12:40'),
(3, 2, 90, 9, '2019-11-04 13:12:40'),
(4, 3, 80, 8, '2019-11-04 13:12:40'),
(5, 4, 70, 7, '2019-11-04 13:12:41'),
(6, 5, 60, 6, '2019-11-04 13:12:41'),
(7, 1, 50, 5, '2019-11-04 13:12:41'),
(8, 2, 40, 4, '2019-11-04 13:12:41'),
(9, 3, 30, 3, '2019-11-04 13:12:41'),
(27, 6, 58, 1, '2019-11-07 20:47:37'),
(28, 7, 48, 1, '2019-11-07 20:47:37'),
(29, 8, 100, 1, '2019-11-07 20:47:37'),
(30, 9, 40, 1, '2019-11-07 20:47:37'),
(31, 10, 60, 1, '2019-11-07 20:47:37'),
(32, 9, 70, 1, '2019-11-07 20:47:37'),
(33, 8, 98, 1, '2019-11-07 20:47:37'),
(34, 7, 68, 1, '2019-11-07 20:47:37'),
(35, 8, 50, 1, '2019-11-07 20:47:37'),
(36, 6, 88, 1, '2019-11-07 20:50:44'),
(37, 7, 78, 1, '2019-11-07 20:50:44'),
(38, 8, 90, 1, '2019-11-07 20:50:44'),
(39, 9, 38, 1, '2019-11-07 20:50:44'),
(40, 10, 68, 1, '2019-11-07 20:50:44'),
(41, 9, 99, 1, '2019-11-07 20:50:44'),
(42, 8, 80, 1, '2019-11-07 20:50:44'),
(43, 7, 70, 1, '2019-11-07 20:50:44'),
(44, 6, 88, 1, '2019-11-07 20:50:56'),
(45, 7, 78, 1, '2019-11-07 20:50:56'),
(46, 8, 90, 1, '2019-11-07 20:50:56'),
(47, 9, 38, 1, '2019-11-07 20:50:56'),
(48, 10, 68, 1, '2019-11-07 20:50:56'),
(49, 9, 99, 1, '2019-11-07 20:50:56'),
(50, 8, 80, 1, '2019-11-07 20:50:56'),
(51, 7, 70, 1, '2019-11-07 20:50:56'),
(52, 6, 90, 1, '2019-11-07 20:50:56'),
(53, 6, 58, 1, '2019-11-07 20:50:56'),
(54, 7, 48, 1, '2019-11-07 20:50:56'),
(55, 8, 100, 1, '2019-11-07 20:50:56'),
(56, 9, 40, 1, '2019-11-07 20:50:56'),
(57, 10, 60, 1, '2019-11-07 20:50:56'),
(58, 9, 70, 1, '2019-11-07 20:50:56'),
(59, 8, 98, 1, '2019-11-07 20:50:56'),
(60, 7, 68, 1, '2019-11-07 20:50:56'),
(61, 6, 50, 1, '2019-11-07 20:50:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_user`
--

CREATE TABLE `log_user` (
  `id_login` int(255) NOT NULL,
  `user_id` varchar(16) NOT NULL,
  `waktu_login` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `reg_user`
--

CREATE TABLE `reg_user` (
  `user_id` varchar(16) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(16) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `kota_tinggal` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `reg_user`
--

INSERT INTO `reg_user` (`user_id`, `nama`, `password`, `email`, `tanggal_lahir`, `kota_tinggal`) VALUES
('1000000000000001', 'Ani Marni', 'asdfjkl', 'animarni@gmail.com', '1990-01-01', 'Bandung'),
('1000000000000002', 'Budi Yanto', 'asdfjkl', 'budiyanto@gmail.com', '1991-02-02', 'Bandung'),
('1000000000000003', 'Charlie Darwin', 'asdfjkl', 'charliedarwin@gmail.com', '1992-03-03', 'Bandung'),
('admin', 'Emilisa', 'halo', 'emilisa90@gmail.com', '1990-06-28', 'Bengkulu');

-- --------------------------------------------------------

--
-- Struktur untuk view `game001_leaderboard`
--
DROP TABLE IF EXISTS `game001_leaderboard`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `game001_leaderboard`  AS  select `reg_user`.`email` AS `email`,`reg_user`.`nama` AS `nama`,`reg_user`.`kota_tinggal` AS `kota`,`leaderboard`.`score` AS `score` from (((`game` join `game_user`) join `leaderboard`) join `reg_user`) where `game`.`game_id` = 1 and `leaderboard`.`game_user_id` = `game_user`.`game_user_id` and `game_user`.`user_id` = `reg_user`.`user_id` and `game_user`.`game_id` = `game`.`game_id` group by `game`.`game_id` order by `leaderboard`.`score` desc ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`game_id`);

--
-- Indeks untuk tabel `game_user`
--
ALTER TABLE `game_user`
  ADD PRIMARY KEY (`game_user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indeks untuk tabel `leaderboard`
--
ALTER TABLE `leaderboard`
  ADD PRIMARY KEY (`id_leaderboard`),
  ADD KEY `game_user_id` (`game_user_id`);

--
-- Indeks untuk tabel `log_user`
--
ALTER TABLE `log_user`
  ADD PRIMARY KEY (`id_login`),
  ADD KEY `user_id` (`user_id`);

--
-- Indeks untuk tabel `reg_user`
--
ALTER TABLE `reg_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `game`
--
ALTER TABLE `game`
  MODIFY `game_id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `game_user`
--
ALTER TABLE `game_user`
  MODIFY `game_user_id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `leaderboard`
--
ALTER TABLE `leaderboard`
  MODIFY `id_leaderboard` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT untuk tabel `log_user`
--
ALTER TABLE `log_user`
  MODIFY `id_login` int(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
